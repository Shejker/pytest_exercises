from pageObjects.HomePage import HomePage
from utilities.SetupClass import SetupClass

class TestE2E(SetupClass):
    
    def test_E2E(self):
        log = self.getLogger()
        # --- HomePage
        homePage = HomePage(self.driver)
        # --- Moving frome HomePage to CheckoutPage
        checkoutpage = homePage.shopItems()
        # --- CheckoutPage
        log.info("Getting all the card titles:")
        cards = checkoutpage.getCardTitles()
        i = -1
        for card in cards:
            i = i + 1
            cardText = card.text
            log.info(cardText)
            if cardText == "Blackberry":
                checkoutpage.getCardFooter()[i].click()

        checkoutpage.goToCheckout()
        # --- Moving from CheckoutPage to ConfirmPage
        confirmpage = checkoutpage.checkOutItems()
        # --- ConfirmPage
        log.info("Entering country name")

        confirmpage.getCountry()
        self.verifyLinkPresence("India")

        confirmpage.chooseCountry()
        confirmpage.acceptRules()
        confirmpage.submitOrder()
        confirmpage.checkOrder()
        log.info("SUCCESS!")
