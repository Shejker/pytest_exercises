from selenium.webdriver.common.by import By

class ConfirmPage:

    def __init__(self, driver):
        self.driver = driver

    country = (By.ID, "country")
    chosen_country = (By.LINK_TEXT, "India")
    checkbox_accept = (By.CSS_SELECTOR, "label[for='checkbox2']")
    submit_order = (By.CSS_SELECTOR, "[type='submit']")
    message = (By.CSS_SELECTOR, "[class*='alert-success']")

    def getCountry(self):
        return self.driver.find_element(*ConfirmPage.country).send_keys("Ind")

    def chooseCountry(self):
        return self.driver.find_element(*ConfirmPage.chosen_country).click()

    def acceptRules(self):
        return self.driver.find_element(*ConfirmPage.checkbox_accept).click()

    def submitOrder(self):
        return self.driver.find_element(*ConfirmPage.submit_order).click()

    def checkOrder(self):
        message = self.driver.find_element(*ConfirmPage.message).text
        assert ("Success! Thank you!" in message)
