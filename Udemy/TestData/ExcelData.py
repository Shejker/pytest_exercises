from typing import Dict
import openpyxl

book = openpyxl.load_workbook("PythonDemo.xlsx")
sheet = book.active

cell = sheet.cell(row=1, column=2)
sheet.cell(row=2, column=2).value = "Tester"
print(cell.value + ": " + sheet.cell(row=2, column=2).value)

print(f"Rows: {sheet.max_row}")

print(f"Columns: {sheet.max_column}")

print(f"A5 value: {sheet['A5'].value}")