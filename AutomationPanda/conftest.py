import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service


@pytest.fixture
def driver():
    service = Service("C:\\bin\\chromedriver.exe")
    option = webdriver.ChromeOptions()
    option.add_argument('--start-maximized')
    option.add_experimental_option('excludeSwitches', ['enable-logging'])
    driver = webdriver.Chrome(service=service, options=option)
    driver.implicitly_wait(10)
    yield driver
    driver.quit()
