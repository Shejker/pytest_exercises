from selenium.webdriver.common.by import By
from pages.results import ResultsPage


class SearchPage:

    URL = 'https://www.duckduckgo.com'
    search_input = (By.ID, 'search_form_input_homepage')
    search_button = (By.ID, 'search_button_homepage')

    def __init__(self, driver):
        self.driver = driver

    def openURL(self):
        self.driver.get(self.URL)

    def search(self, phrase):
        self.driver.find_element(*SearchPage.search_input).send_keys(phrase)
        resultPage = ResultsPage(self.driver.find_element(*SearchPage.search_button).click())
        return resultPage
