from pytest_bdd import scenarios, given, when, then, parsers
from pages.search import SearchPage
from pages.results import ResultsPage

scenarios("../features/web.feature")
phrase = 'panda'

@given('the DuckDuckGo home page is displayed')
def ddg_home(driver):
    searchPage = SearchPage(driver)
    searchPage.openURL()

@when(parsers.parse('the user searches for "{phrase}"'))
def search_phrase(driver):
    searchPage = SearchPage(driver)
    searchPage.search(phrase)

@then(parsers.parse('results are shown for "{phrase}"'))
def search_results(driver):
    resultPage = ResultsPage(driver)
    assert resultPage.linkDivCount() > 0
    assert resultPage.phraseResultsCount(phrase) > 0
    assert phrase in resultPage.searchInputValue()
