import json
import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service

CONFIG_PATH = 'conf.json'
DEFAULT_WAIT_TIME = 10
SUPPORTED_BROWSERS = ['chrome', 'firefox']

@pytest.fixture(scope='session')
def config_browser(config):
    if 'browser' not in config:
        raise Exception('The config file does not contain "browser"')
    elif config['browser'] not in SUPPORTED_BROWSERS:
        raise Exception(f'"{config["browser"]}" is not a supported browser')
    return config['browser']

@pytest.fixture(scope='session')
def config_wait_time(config):
    return config['wait_time'] if 'wait_time' in config else DEFAULT_WAIT_TIME

@pytest.fixture(scope='session')
def config():
    with open(CONFIG_PATH) as config_file:
        data = json.load(config_file)
    return data

@pytest.fixture
def browser(config_browser, config_wait_time):
    if config_browser == 'chrome':
        service = Service("C:\\bin\\chromedriver.exe")
        option = webdriver.ChromeOptions()
        option.add_argument('--start-maximized')
        option.add_experimental_option('excludeSwitches', ['enable-logging'])
        driver = webdriver.Chrome(service=service, options=option)
    elif config_browser == 'firefox':
        service = Service("C:\\bin\\geckodriver.exe")
        options = webdriver.FirefoxOptions()
        driver = webdriver.Firefox(service=service, options=options)
        driver.maximize_window()
    else:
        raise Exception(f'"{config_browser}" is not a supported browser')
    driver.implicitly_wait(config_wait_time)
    yield driver
    driver.close()
