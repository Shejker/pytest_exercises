from selenium.webdriver.common.by import By

class ResultsPage:

    link_divs = (By.CSS_SELECTOR, '#links > div')
    search_input = (By.ID, 'search_form_input')

    def __init__(self, driver):
        self.driver = driver

    @classmethod
    def phraseResult(self, phrase):
        xpath = f"//div[@id='links']//*[contains(text(), '{phrase}')]"
        return(By.XPATH, xpath)

    def linkDivCount(self):
        link_divs = self.driver.find_elements(*ResultsPage.link_divs)
        return len(link_divs)
    
    def phraseResultsCount(self, phrase):
        phrase_results = self.driver.find_elements(*ResultsPage.phraseResult(phrase))
        return len(phrase_results)
    
    def searchInputValue(self):
        search_input = self.driver.find_element(*ResultsPage.search_input)
        return search_input.get_attribute('value')
