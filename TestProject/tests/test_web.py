from pages.results import ResultsPage
from pages.search import SearchPage

def test_basic_duckduckgo_search(browser):

    PHRASE = 'panda'

    searchPage = SearchPage(browser)
    searchPage.openURL()
    resultPage = searchPage.search(PHRASE)
    
    resultPage = ResultsPage(browser)
    assert resultPage.linkDivCount() > 0
    assert resultPage.phraseResultsCount(PHRASE) > 0
    assert PHRASE in resultPage.searchInputValue()
