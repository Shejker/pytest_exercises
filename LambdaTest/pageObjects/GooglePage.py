from selenium.webdriver.common.by import By


class GooglePage:
    
    def __init__(self, driver):
        self.driver = driver

    rules = (By.CSS_SELECTOR, "#L2AGLb > div")
    search_bar = (By.XPATH, "//input[@name='q']")
    text = "LambdaTest"
    submit = (By.XPATH, "//body/div[1]/div[3]/form[1]/div[1]/div[1]/div[3]/center[1]/input[1]")
    choose_link = (By.PARTIAL_LINK_TEXT, "LambdaTest")
    
    def acceptRules(self):
        return self.driver.find_element(*GooglePage.rules).click()

    def checkTitle(self, title):
        assert self.driver.title == title
    
    def inputText(self):
        return self.driver.find_element(*GooglePage.search_bar).send_keys(self.text)

    def sumbitForm(self):
        return self.driver.find_element(*GooglePage.submit).click()
    
    def chooseSite(self):
        return self.driver.find_element(*GooglePage.choose_link).click()
