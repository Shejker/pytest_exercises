from selenium.webdriver.common.by import By
from pageObjects.GooglePage import GooglePage


class LambdaPage:

    def __init__(self, driver):
        self.driver = driver

    lambda_page = "https://lambdatest.github.io/sample-todo-app/"
    title = "Sample page - lambdatest.com"
    text = "Happy Testing at LambdaTest"
    checkbox1 = (By.NAME, "li1")
    checkbox2 = (By.NAME, "li2")
    input = (By.ID, "sampletodotext")
    submit = (By.ID,"addbutton")
    form_class = (By.XPATH, "//body/div[1]/div[1]/div[1]")
    google_page = "https://www.google.com/"

    def setPage(self):
        return self.driver.get(self.lambda_page)

    def checkTitle(self):
        assert self.driver.title == self.title

    def clickCheckboxes(self):
        return self.driver.find_element(*LambdaPage.checkbox1).click(), self.driver.find_element(*LambdaPage.checkbox2).click()
    
    def inputText(self):
        return self.driver.find_element(*LambdaPage.input).send_keys(self.text)

    def sumbitForm(self):
        return self.driver.find_element(*LambdaPage.submit).click()

    def checkAddedOption(self):
        assert self.text in self.driver.find_element(*LambdaPage.form_class).text

    def moveToGoogle(self):
        googlePage = GooglePage(self.driver)
        self.driver.get(self.google_page)
        return googlePage
