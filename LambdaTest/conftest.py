import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service


@pytest.fixture(params=["chrome", "firefox"], scope="class")
def setup(request):
    if request.param == "chrome":
        service = Service("C:\\bin\\chromedriver.exe")
        options = webdriver.ChromeOptions()
        options.add_argument('--start-maximized')
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        driver = webdriver.Chrome(service=service, options=options)
    if request.param == "firefox":
        service = Service("C:\\bin\\geckodriver.exe")
        options = webdriver.FirefoxOptions()
        driver = webdriver.Firefox(service=service, options=options)
        driver.maximize_window()
    request.cls.driver = driver
    yield
    driver.close()
