import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service


@pytest.mark.parametrize(
    "test_browser, test_url",
    [
        ("chrome", "https://www.lambdatest.com/"),
        ("firefox", "https://www.lambdatest.com/blog/"),
        pytest.param("IE", "https://www.lambdatest.com/blog/", marks=pytest.mark.skip),
        pytest.param("safari", "https://www.lambdatest.com/blog/", marks=pytest.mark.skip),
    ]
)

def test_open_url(test_url, test_browser):
    if test_browser == "chrome":
        service = Service("C:\\bin\\chromedriver.exe")
        options = webdriver.ChromeOptions()
        options.add_argument('--start-maximized')
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        driver = webdriver.Chrome(service=service, options=options)
        expected_title = "Most Powerful Cross Browser Testing Tool Online | LambdaTest"
    if test_browser == "firefox":
        service = Service("C:\\bin\\geckodriver.exe")
        options = webdriver.FirefoxOptions()
        options.add_argument('--start-maximized')
        driver = webdriver.Firefox(service=service, options=options)
        expected_title = "LambdaTest Blogs"
 
    driver.get(test_url)
    driver.maximize_window()
    
    assert expected_title == driver.title
    
    driver.close()
