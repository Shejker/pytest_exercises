import pytest
from pageObjects.LambdaPage import LambdaPage


@pytest.mark.usefixtures("setup")
class Test_URL():

    def test_lambda(self):

        lambdaPage = LambdaPage(self.driver)
        lambdaPage.setPage()
        lambdaPage.checkTitle()
        lambdaPage.clickCheckboxes()
        lambdaPage.inputText()
        lambdaPage.sumbitForm()
        lambdaPage.checkAddedOption()

    def test_google(self):
        lambdaPage = LambdaPage(self.driver)
        googlePage = lambdaPage.moveToGoogle()
        googlePage.acceptRules()
        googlePage.checkTitle("Google")
        googlePage.inputText()
        googlePage.sumbitForm()
        googlePage.chooseSite()
        googlePage.checkTitle("Most Powerful Cross Browser Testing Tool Online | LambdaTest")
